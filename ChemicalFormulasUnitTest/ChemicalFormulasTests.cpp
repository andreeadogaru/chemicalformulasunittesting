#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChemicalFormula.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ChemicalFormulasUnitTest
{
	TEST_CLASS(ChemicalFormulasTests)
	{
	public:

		TEST_METHOD(OneLetterOneElementOneAtom)
		{
			ChemicalFormula chemicalFormula("S");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["S"] = 1;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(TwoLettersOneElementOneAtom)
		{
			ChemicalFormula chemicalFormula("Fe");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["Fe"] = 1;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(OneLetterOneElementTwoAtoms)
		{
			ChemicalFormula chemicalFormula("O2");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["O"] = 2;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(TwoLettersOneElementTwoAtoms)
		{
			ChemicalFormula chemicalFormula("Ca2");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["Ca"] = 2;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(OneLetterTwoElementsOneAtom)
		{
			ChemicalFormula chemicalFormula("CN");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["C"] = 1;
			atoms["N"] = 1;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(TwoLettersTwoElementsOneAtom)
		{
			ChemicalFormula chemicalFormula("CaCl");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["Ca"] = 1;
			atoms["Cl"] = 1;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(NoBracketsMultipleAtoms)
		{
			ChemicalFormula chemicalFormula("C2H5Br");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["C"] = 2;
			atoms["H"] = 5;
			atoms["Br"] = 1;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(OneBracketSurroundedGroup)
		{
			ChemicalFormula chemicalFormula("(SO4)3");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["S"] = 3;
			atoms["O"] = 12;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(OneElementOneBracketSurroundedGroup)
		{
			ChemicalFormula chemicalFormula("Fe(OH)3");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["Fe"] = 1;
			atoms["O"] = 3;
			atoms["H"] = 3;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(OneBracketSurroundedGroupMultipleElements)
		{
			ChemicalFormula chemicalFormula("(NH4)3PO4");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["N"] = 3;
			atoms["H"] = 12;
			atoms["P"] = 1;
			atoms["O"] = 4;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(MultipleBracketSurroundedGroupMultipleElements)
		{
			ChemicalFormula chemicalFormula("Fe(NCl)3(H2O)9");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["Fe"] = 1;
			atoms["N"] = 3;
			atoms["Cl"] = 3;
			atoms["O"] = 9;
			atoms["H"] = 18;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}

		TEST_METHOD(ThreeLettersElement)
		{
			Assert::ExpectException<std::exception>(
				[]() {
				ChemicalFormula chemicalFormula("Mgn");
			});
		}

		TEST_METHOD(BracketsNotClosed)
		{
			Assert::ExpectException<std::exception>(
				[]() {
				ChemicalFormula chemicalFormula("Fe2(Mg3N");
			});
		}

		TEST_METHOD(BracketsNotOpened)
		{
			Assert::ExpectException<std::exception>(
				[]() {
				ChemicalFormula chemicalFormula("Fe2CO2)3");
			});
		}

		TEST_METHOD(NoChemicalElement)
		{
			Assert::ExpectException<std::exception>(
				[]() {
				ChemicalFormula chemicalFormula("3");
			});
		}

		TEST_METHOD(RepeatingElement)
		{
			ChemicalFormula chemicalFormula("Al2SO4(CH3CO2)4");
			std::unordered_map<std::string, uint8_t> atoms;
			atoms["Al"] = 2;
			atoms["S"] = 1;
			atoms["O"] = 12;
			atoms["C"] = 8;
			atoms["H"] = 12;
			Assert::IsTrue(chemicalFormula.getAtoms() == atoms);
		}
	};
}