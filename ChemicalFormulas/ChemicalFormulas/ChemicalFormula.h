#pragma once
#include <regex>
#include <string>
#include <unordered_map>

class ChemicalFormula
{
public:
	ChemicalFormula(const std::string& molecule);
	const std::unordered_map<std::string, uint8_t>& getAtoms() const;
private:
	void parse(const std::string& leftToParse, uint8_t multiplier = 1);
	void addAtom(const std::string& atom, uint8_t multiplier = 1);
private:
	std::string m_molecule;
	std::unordered_map<std::string, uint8_t> m_atoms;
};