#include "ChemicalFormula.h"

ChemicalFormula::ChemicalFormula(const std::string & molecule) :
	m_molecule(molecule)
{
	parse(molecule, 1);
}

const std::unordered_map<std::string, uint8_t>& ChemicalFormula::getAtoms() const
{
	return m_atoms;
}

void ChemicalFormula::parse(const std::string & leftToParse, uint8_t multiplier)
{
	if (leftToParse != std::string())
	{
		std::regex atom("([A-Z][a-z]?[0-9]*)(.*)");
		std::regex bracketsGroup("(\\((.*?)\\)([0-9]*))(.*)");
		std::smatch matched;
		if (std::regex_match(leftToParse, matched, atom))
		{
			addAtom(static_cast<std::ssub_match>(matched[1]).str(), multiplier);
			parse(static_cast<std::ssub_match>(matched[2]).str(), multiplier);
		}
		else
			if (std::regex_match(leftToParse, matched, bracketsGroup))
			{
				std::string sMultiply = static_cast<std::ssub_match>(matched[3]).str();
				uint8_t multiply = 1;
				if (sMultiply != std::string())
				{
					multiply = std::stoi(sMultiply);
				}
				parse(static_cast<std::ssub_match>(matched[2]).str(), multiplier*multiply);
				parse(static_cast<std::ssub_match>(matched[4]).str(), multiplier);
			}
			else
			{
				throw std::exception("Incorrect formula.");
			}
	}
}

void ChemicalFormula::addAtom(const std::string & atom, uint8_t multiplier)
{
	std::regex elementRegex("([A-Z][a-z]?)([0-9]*)");
	std::smatch elementMatch;
	std::regex_match(atom, elementMatch, elementRegex);
	uint8_t count = 1;
	if (elementMatch.size() == 3)
	{
		std::string sCount = static_cast<std::ssub_match>(elementMatch[2]).str();
		if (sCount != std::string())
		{
			count = std::stoi(sCount);
		}
	}
	count *= multiplier;
	std::string element(static_cast<std::ssub_match>(elementMatch[1]).str());
	if (m_atoms.find(element) != m_atoms.end())
		m_atoms[element] += count;
	else m_atoms[element] = count;
}
